package main

import (
	"log"
	"time"
)

func readws() {
	for {
		if cws {
			var wsjson wspacket
			err := ws.Socket.ReadJSON(&wsjson)
			if err != nil {
				log.Println("dial:", err)
				cws = false
			}

			if wsjson.Type == "c" {
				log.Printf("command: %s", wsjson.Message)
				telcontrol.Write([]byte(wsjson.Message + "\n"))
			} else if wsjson.Type == "p" {
				ws.send(wspacket{Type: "p", Message: "p"})
			}
		} else {
			time.Sleep(5 * time.Second)
		}
	}
}
