package main

import (
	"log"
	"time"
)

func readtelcontrol() {
	var command string
	var buffer [1]byte // Seems like the length of the buffer needs to be small, otherwise will have to wait for buffer to fill up.
	p := buffer[:]

	for {
		if ctc {
			// Read 1 byte.
			n, err := telcontrol.Read(p)
			if n <= 0 && nil == err {
				continue
			} else if n <= 0 && nil != err {
				break
			}
			if string(p) == "\n" {
				log.Println("telcontrol: " + command)
				wsjson := wspacket{Type: "c", Message: string(command)}
				ws.send(wsjson)
				command = ""
			} else {
				command = command + string(p)
			}
		} else {
			time.Sleep(5 * time.Second)
		}
	}
}

func readtelstatus() {
	var command string
	var buffer [1]byte // Seems like the length of the buffer needs to be small, otherwise will have to wait for buffer to fill up.
	p := buffer[:]

	for {
		if cts {
			// Read 1 byte.
			n, err := telstatus.Read(p)
			if n <= 0 && nil == err {
				continue
			} else if n <= 0 && nil != err {
				break
			}
			if string(p) == "\n" {
				log.Println("telstatus: " + command)
				wsjson := wspacket{Type: "s", Message: string(command)}
				ws.send(wsjson)
				command = ""
			} else {
				command = command + string(p)
			}
		} else {
			time.Sleep(5 * time.Second)
		}
	}
}
