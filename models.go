package main

import (
	"log"
	"sync"

	"github.com/gorilla/websocket"
)

type configjson struct {
	Cbus struct {
		Address string `json:"address"`
		Port    string `json:"port"`
		Status  string `json:"status"`
	} `json:"cbus"`
	SSL    bool   `json:"ssl"`
	Server string `json:"server"`
	Path   string `json:"path"`
	WSpath string `json:"wspath"`
	Token  string `json:"token"`
}

type wspacket struct {
	Type    string `json:"t"`
	Message string `json:"m"`
}

type wsconn struct {
	Socket *websocket.Conn // websocket connection of the player
	mu     sync.Mutex
}

func (conn *wsconn) send(p wspacket) {
	if cws {
		conn.mu.Lock()
		defer conn.mu.Unlock()
		err := conn.Socket.WriteJSON(p)
		if err != nil {
			log.Println(err)
			cws = false
		}
	}
}
