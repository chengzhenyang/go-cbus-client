module go-cbus-client

go 1.12

require (
	github.com/gorilla/websocket v1.4.1
	github.com/reiver/go-oi v1.0.0
	github.com/reiver/go-telnet v0.0.0-20180421082511-9ff0b2ab096e
)
