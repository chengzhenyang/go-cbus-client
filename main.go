package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"net/url"
	"os"
	"time"

	"github.com/gorilla/websocket"
	"github.com/reiver/go-telnet"
)

type caller struct{}

var telcontrol *telnet.Conn
var telstatus *telnet.Conn
var ws wsconn
var cws, ctc, cts = false, false, false

var config configjson

const (
	setupURL = "https://www.tsatech.nz/cbus/setupclient"
)

func main() {
	fmt.Println("\nBooting")
	setup()

	for !ctc && !cts && !cws {
		if !ctc {
			ctc = connecttelcontrol()
			checkbool(cts, "telcontrol")
		}
		if !cts {
			cts = connecttelstatus()
			checkbool(cts, "telstatus")
		}
		if !cws && cts && cws {
			cws = connectwebsocket()
			checkbool(cws, "websocket")
		}
		time.Sleep(3 * time.Second)
	}

	go readtelcontrol()
	go readtelstatus()
	go readws()
	go connect()
	select {}
}

func setup() {
	configFile, err := os.Open("config.json")
	defer configFile.Close()
	if err != nil {
		log.Println("No config found, setting up config using")
		r, err := http.Get(setupURL)
		decoder := json.NewDecoder(r.Body)
		err = decoder.Decode(&config)
		if err != nil {
			log.Fatal("setup failed")
		}
		file, _ := json.MarshalIndent(config, "", "	")
		_ = ioutil.WriteFile("config.json", file, 0644)
	} else {
		jsonParser := json.NewDecoder(configFile)
		err = jsonParser.Decode(&config)
		if err != nil {
			log.Fatal("failed to load config")
		}
	}
}

func connect() {
	for {
		if !ctc {
			ctc = connecttelcontrol()
			checkbool(cts, "telcontrol")
		}
		if !cts {
			cts = connecttelstatus()
			checkbool(cts, "telstatus")
		}
		if !cws {
			cws = connectwebsocket()
			checkbool(cws, "websocket")
		}
		time.Sleep(5 * time.Second)
	}
}

func checkbool(state bool, name string) {
	if state {
		log.Println("connecting to " + name)
	} else {
		log.Println("failed to " + name)
	}
}

func connectwebsocket() bool {
	var wsscheme = "ws"
	if config.SSL {
		wsscheme = "wss"
	}
	var err error
	// init websocket
	u := url.URL{Scheme: wsscheme, Host: config.Server, Path: config.Path + config.WSpath + "/" + config.Token}
	log.Printf("Connecting to %s", u.String())
	ws.Socket, _, err = websocket.DefaultDialer.Dial(u.String(), nil)
	if err != nil {
		log.Println("error wss: ", err)
		return false
	}
	ifaces, err := net.Interfaces()
	// handle err
	for _, i := range ifaces {
		addrs, _ := i.Addrs()
		// handle err
		for _, addr := range addrs {
			var ip net.IP
			switch v := addr.(type) {
			case *net.IPNet:
				ip = v.IP
			case *net.IPAddr:
				ip = v.IP
			}
			// process IP address
			ws.send(wspacket{Type: "s", Message: "ip: " + ip.String()})
		}
	}
	return true
}

func connecttelcontrol() bool {
	// init telnet control
	var err error
	log.Printf("Dial to Control %s:%s \n", config.Cbus.Address, config.Cbus.Port)
	telcontrol, err = telnet.DialTo(fmt.Sprintf("%s:%s", config.Cbus.Address, config.Cbus.Port))
	if err != nil {
		log.Println(err)
		return false
	}
	return true
}
func connecttelstatus() bool {
	// init telnet status
	var err error
	log.Printf("Dial to Status %s:%s \n", config.Cbus.Address, config.Cbus.Status)
	telstatus, err = telnet.DialTo(fmt.Sprintf("%s:%s", config.Cbus.Address, config.Cbus.Status))
	if err != nil {
		log.Println(err)
		return false
	}
	return true
}
